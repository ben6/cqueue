/* Copyright (C) 2017 Ben Wei<ben@juluos.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <stdio.h>
#include <errno.h>
#include <string.h> /* memcpy */
#include <stdlib.h>
#include <sys/mman.h> /* mmap */
#include <sys/types.h> /* open */
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h> /* signal */
#include <unistd.h>
#include <string.h>

#include <semaphore.h>
#include "cqueue.h"

void hexdump (void *addr, int len);
unsigned int max_frame_size = 8;
#define MAX_FRAMES  5

#define MAX_MAP_SIZE 204800

static int g_running = 1;
static cqueue_t g_cqueue = {0};
#define NON_ASCII_DATA "non ascii data"
char rbuf[MAX_MAP_SIZE] = {0}; 

void *process_single_worker(void *arg)
{
    unsigned long loop_count = 1;
    cfrm_t *f = NULL;
    cqueue_t *q = (cqueue_t *) arg;
    pid_t pid = getpid();
    printf("%s started (tid=%u) cq_mem=%p\n", __FUNCTION__, pid, q->mem);
    unsigned int sleep_time = 0;
    while (g_running)
    {
        f = cq_dequeue(&g_cqueue);
        printf("%lu de:%p seq:%u size:%u d:%s\n",
                    loop_count, f, f->seq, f->size, f->data);

        cq_free_item(&g_cqueue, f);
        printf("> count:%u frees:%u\n",
                    g_cqueue.count,
                    g_cqueue.free_count);

        if (loop_count == 15) {
            sleep_time = 250;
        } else if (loop_count == 45) {
            sleep_time = 500;
        } else if (loop_count > 60) {
            sleep_time = 0;
        }
        usleep(sleep_time);
        loop_count++;
    }
    printf("%s exited (tid=%u)\n", __FUNCTION__, pid);
    pthread_exit(0);
    return NULL;
}

/* map size is too big, pull out from function 
 * avoid overflow of function stack
 **/
int test_add_item(cqueue_t *q, unsigned int i, unsigned int max_frame_size);

void *process_producer(void *arg)
{
    int i = 0, rc = 0;
    cqueue_t *q = (cqueue_t *) arg;
    pid_t pid = getpid();

    printf("%s started (tid=%u) cq_mem=%p\n", __FUNCTION__, pid, q->mem);
    for (; i < 10; i++) {
        rc = test_add_item(&g_cqueue, i, max_frame_size);
        if (rc != 0) 
            printf("fail to add %u\n", i);
        else
            printf("ok to add %u\n", i);
    }

    while (g_running)
    {
        rc = test_add_item(&g_cqueue, i++, max_frame_size);
        if (rc != 0) 
            printf("fail to add %u\n", i);
        else
            printf("ok to add %u\n", i);

        if (i > 30 && i < 80)
            usleep(20);
        else if (i > 80)
            usleep(500);

        if (i > 100) {
            g_running = 0;
            cq_stop_loop(&g_cqueue);
        }
    }
    printf("%s exited (tid=%u)\n", __FUNCTION__, pid);

    pthread_exit(0);

    return 0;
}


static void signal_handler(int signal)
{
    if (signal == SIGINT)
    {
        printf("received user interrupt\n");
        g_running = 0;
        cq_stop_loop(&g_cqueue);
    }
}

int test_add_item(cqueue_t *q, unsigned int i, unsigned int max_frame_size)
{
    int rc = -1;
    cfrm_t *f = cq_new_item(q);
    if (!f) {
        printf("no free item c:%u f:%u\n", q->count, q->free_count);
        return rc;
    }

    f->seq = i+1;
    f->size = snprintf((char *)f->data, max_frame_size, "test%u", i);
    rc = cq_enqueue(q, f);
    if (rc) {
        printf("en rc=%d\n", rc);
        cq_free_item(q, f);
    } else {
        printf("en f=%p\n", f);
    }
    return rc;
}


int test_cqueue_basic(void)
{
    cfrm_t *f = NULL;
    int id = 1;
    int rc = 0;

    rc = cq_init(&g_cqueue, id, MAX_FRAMES, max_frame_size, 0);
    if (rc) {
        printf("cqueue_init rc=%d\n", rc);
        return rc;
    }

    cq_set_nonblocking(&g_cqueue, 1);
 
    test_add_item(&g_cqueue, 0, max_frame_size);
    test_add_item(&g_cqueue, 1, max_frame_size);
    test_add_item(&g_cqueue, 2, max_frame_size);
    test_add_item(&g_cqueue, 3, max_frame_size);
    test_add_item(&g_cqueue, 4, max_frame_size);
    test_add_item(&g_cqueue, 5, max_frame_size);

    do {
        f = cq_dequeue(&g_cqueue);
        if (f) {
            printf("seq:%u size:%u d:%s\n",
                    f->seq, f->size, f->data);

            cq_free_item(&g_cqueue, f);
            printf("count:%u frees:%u\n",
                    g_cqueue.count,
                    g_cqueue.free_count);
        }
    } while (f);
    cq_set_nonblocking(&g_cqueue, 0);

    return 0;
}

int test_threads(void)
{
    int rc = 0;
    void *value = NULL;
    pthread_t tid_producer;
    pthread_t tid_worker;

    rc = pthread_create(&tid_worker, NULL, process_single_worker, &g_cqueue);
    if (rc) {
        printf("pthread_create worker rc=%d\n", rc);
        return rc;
    }

    rc = pthread_create(&tid_producer, NULL, process_producer, &g_cqueue);
    if (rc) {
        printf("pthread_create producer rc=%d\n", rc);
        return rc;
    }

    pthread_join(tid_worker, &value);
    pthread_join(tid_producer, &value);


    return 0;
}

void *process_single_worker_stress(void *arg)
{
    unsigned long loop_count = 1;
    cfrm_t *f = NULL;
    cqueue_t *q = (cqueue_t *) arg;
    pid_t pid = getpid();
    printf("%s started (tid=%u) cq_mem=%p\n", __FUNCTION__, pid, q->mem);
    while (g_running)
    {
        f = cq_dequeue(&g_cqueue);
        if (f) {
            printf("%lu de:%p seq:%u size:%u d:%s\n",
                    loop_count, f, f->seq, f->size, f->data);

            cq_free_item(&g_cqueue, f);
            printf("> count:%u frees:%u\n",
                    g_cqueue.count,
                    g_cqueue.free_count);
        }
        loop_count++;
    }
    printf("%s exited (tid=%u)\n", __FUNCTION__, pid);
    pthread_exit(0);
    return NULL;
}

void *process_producer_stress(void *arg)
{
    int i = 0, rc = 0;
    cqueue_t *q = (cqueue_t *) arg;
    pid_t pid = getpid();

    printf("%s started (tid=%u) cq_mem=%p\n", __FUNCTION__, pid, q->mem);

    while (g_running)
    {
        rc = test_add_item(&g_cqueue, i++, max_frame_size);
        if (rc != 0) 
            printf("fail to add %u\n", i);
        else
            printf("ok to add %u\n", i);
        usleep(1);

        if (i > 10000) {
            g_running = 0;
            cq_stop_loop(&g_cqueue);
        }
    }
    printf("%s exited (tid=%u)\n", __FUNCTION__, pid);

    pthread_exit(0);

    return 0;
}

int test_threads_new_free_stress(void)
{
    int rc = 0;
    void *value = NULL;
    pthread_t tid_producer;
    pthread_t tid_worker;

    rc = pthread_create(&tid_worker, NULL, process_single_worker_stress, &g_cqueue);
    if (rc) {
        printf("pthread_create worker rc=%d\n", rc);
        return rc;
    }

    rc = pthread_create(&tid_producer, NULL, process_producer_stress, &g_cqueue);
    if (rc) {
        printf("pthread_create producer rc=%d\n", rc);
        return rc;
    }

    pthread_join(tid_worker, &value);
    pthread_join(tid_producer, &value);

    return 0;
}

void *process_stress(void *arg)
{
    int i = 0;
    cqueue_t *q = (cqueue_t *) arg;
    pid_t pid = getpid();
    cfrm_t *items[MAX_FRAMES];


    printf("%s started (tid=%u) cq_mem=%p\n", __FUNCTION__, pid, q->mem);

    while (g_running)
    {
        for (i = 0 ; i < MAX_FRAMES; i++) {
            items[i] = cq_new_item(q);
        }

        for (i = 0 ; i < MAX_FRAMES; i++) {
            cq_free_item(q, items[i]);
        }

        if (i > 10000) {
            g_running = 0;
            cq_stop_loop(&g_cqueue);
        }
    }
    printf("%s exited (tid=%u)\n", __FUNCTION__, pid);

    pthread_exit(0);

    return 0;
}

int test_process_new_free_item_stress(void)
{
    int rc = 0;
    void *value = NULL;
    pthread_t tid_worker;

    rc = pthread_create(&tid_worker, NULL, process_stress, &g_cqueue);
    if (rc) {
        printf("pthread_create worker rc=%d\n", rc);
        return rc;
    }

    pthread_join(tid_worker, &value);

    return 0;
}

int main(int argc, char **argv)
{
    int rc = 0;

    signal(SIGINT, &signal_handler);

    test_cqueue_basic();
#if 1
    test_threads();
    //test_threads_new_free_stress();
#else
    test_process_new_free_item_stress();
#endif
    printf("free cqueue\n");
    cq_release(&g_cqueue);
    return rc;
}
