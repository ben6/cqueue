PREFIX     ?= ./.libs

LIB_CFILES  = cqueue.c
LIB_OBJS    = $(LIB_CFILES:.c=.o)

CFLAGS     += -g -I. -Wall -I.. 
CFILES      = main.c hexdump.c
OBJS        = $(CFILES:.c=.o)
TARGET      = $(OUTPUT_TEST_DIR)test_cq

CQ_NAME     = cqueue
CQGERA      = $(PREFIX)/lib$(CQ_NAME).a
CQGERSO     = lib$(CQ_NAME).so
CQ_CFLAGS   = -fPIC
LDFLAGS     = -pthread

TARGETS = $(CQGERA)

.PHONY: tests

all: $(TARGETS)

$(OBJS): %.o : %.c
	@echo "CC [$@]" ; \
	$(CC) -c -o $@ $< $(CFLAGS)

$(LIB_OBJS): %.o : %.c
	@echo "CC [$@]" ; \
	$(CC) -fPIC -c -o $@ $< $(CFLAGS)

$(CQGERSO): $(LIB_OBJS)
	@echo "LK [$@]" ; \
	$(CC) -shared -o $@ $^

$(CQGERA): $(LIB_OBJS)
	@echo "LK [$@]" ; \
	mkdir -p "$(dir $@)" ; \
	$(AR) cru -o $@ $^

$(TARGET): $(OBJS) $(CQGERA)
	@echo "LK [$@]" ; \
	$(CC) -o $@ $^ $(LDFLAGS)

run: $(TARGET)
	$(RUNOP) ./$^  $(ARGS)

runv: $(TARGET)
	valgrind --leak-check=full $(ARGS) ./$^ ; \

clean:
	rm -f $(TARGET) $(CQGERA) $(CQGERSO) *.o 

