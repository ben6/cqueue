#include <stdio.h>

#define MAX_HEX_BUF_SIZE 49
#define MAX_ASCII_BUF_SIZE 17

void hexdump (void *addr, int len)
{
    int i;
    int n = 0;
    size_t offset = 0;
    char hexbuf[MAX_HEX_BUF_SIZE] = {0};
    unsigned char ascbuf[MAX_ASCII_BUF_SIZE] = {0};
    unsigned char *p = (unsigned char*)addr;

    if (len == 0 || len < 0) {
        return;
    }

    for (i = 0; i < len; i++)
    {
        int remains = i % 16;
        if (remains == 0) {
            if (i != 0) {
                fprintf(stderr, "%s %s\n", hexbuf, ascbuf);
                offset = 0;
            }
            fprintf(stderr, "  %04x ", i);
        }

        n = snprintf(hexbuf + offset, MAX_HEX_BUF_SIZE - offset, " %02x", p[i]);
        offset += n;

        if ((p[i] < ' ') || (p[i] > 0x7e))
            ascbuf[remains] = '.';
        else
            ascbuf[remains] = p[i];
        ascbuf[remains + 1] = '\0';
    }

    while ((i++ % 16) != 0)
    {
        n = snprintf(hexbuf + offset, MAX_HEX_BUF_SIZE - offset, "   ");
        offset += n;
    }

    fprintf(stderr, "%s %s\n", hexbuf, ascbuf);
}

#ifdef TEST_MAIN

int main()
{
    char sample[] = "\x02\x09\xFFhello, my hexdump\n \tthis is a dummy testing data: 0123456789\n";
    hexdump(sample, sizeof(sample));
    return 0;
}

#endif /* TEST_MAIN */
