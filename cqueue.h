/* Copyright (C) 2017 Ben Wei<ben@juluos.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef CQUEUE_H
#define CQUEUE_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>

#include <sys/time.h> /* struct timeval */

#define U_1KB  1024

#define CODEC_NAME_SIZE 7

typedef struct _cfrm_t
{
    unsigned size;        /* size of data */
    unsigned seq;
    unsigned truncates;   /* num truncates bytes */
    struct timeval ptime; /* presentation time */
    unsigned duration;    /* duration in micro seconds */
    struct _cfrm_t *next;
    char codec_name[CODEC_NAME_SIZE+1];
    unsigned char nal_type;
    unsigned char data[0];
} cfrm_t;

typedef struct
{
    cfrm_t *head;
    cfrm_t *tail;
    cfrm_t *free_head;
    cfrm_t *free_tail;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    unsigned long mem_size;
    unsigned long max_frame_size;
    unsigned int  max_frames;     /* pre-allocate number of frames */
    unsigned int  count;          /* used */
    unsigned int  free_count;
    unsigned int  alloc_count; /* use malloc */
    unsigned char id;
    unsigned char running;
    unsigned char nonblocking;
    unsigned char *mem;
} cqueue_t;

static inline void cq_set_nonblocking(cqueue_t *q, unsigned char v)
{
    q->nonblocking = v;
}

#define CQ_EXIT  1
#define CQ_FULL  1
#define CQ_EMPTY 0

int cq_init(cqueue_t *q, unsigned char id,
        unsigned int max_frames,
        unsigned int size_per_frame,
        int op);

cfrm_t *cq_new_item(cqueue_t *q);

int cq_free_item(cqueue_t *q, cfrm_t *frame);

int cq_enqueue(cqueue_t *q, cfrm_t *frame);
cfrm_t *cq_dequeue(cqueue_t *q);

/* this api will also clear queue */
void cq_stop_loop(cqueue_t *q);

void cq_clear(cqueue_t *q);

int cq_release(cqueue_t *q);

#ifdef __cplusplus
}
#endif

#endif /* CQUEUE_H */
