/* Copyright (C) 2017 Ben Wei<ben@juluos.org>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <time.h> /* pthread_timestruc_t */
#include <pthread.h>
#include <errno.h>
#include <string.h> /* memcpy */
#include <stdlib.h>
#include <sys/mman.h> /* mmap */
#include <sys/types.h> /* open */
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <semaphore.h>
#include "cqueue.h"

#ifdef HAVE_FUNC_DEBUG
#define CENTER_FUNC() printf("%s Enter\n", __FUNCTION__)
#define CEXIT_FUNC(r) printf("%s Exit(rc=%d)\n", __FUNCTION__, r)
#else
#define CENTER_FUNC()
#define CEXIT_FUNC(r)
#endif /* HAVE_FUNC_DEBUG */

#ifdef HAVE_CQ_DEBUG
#define CQ_DEBUG(fmt, args ...)   TDEBUG(fmt, ##args)
#else
#define CQ_DEBUG(fmt, args ...)
#endif

int cq_init(cqueue_t *q, unsigned char id, unsigned int max_frames, unsigned int size_per_frame, int op)
{
    cfrm_t *f, *n = NULL;
    unsigned i = 0;
    unsigned int item_size = sizeof(cfrm_t)+size_per_frame;
    memset(q, 0, sizeof(cqueue_t));
    q->max_frame_size = size_per_frame;
    q->max_frames = max_frames;

    q->id = id;
    pthread_mutex_init(&q->mutex, NULL);
    pthread_cond_init(&q->cond, NULL);

    q->mem_size = max_frames * item_size;
    q->mem = (unsigned char *) malloc(q->mem_size);
    if (q->mem == NULL)
        return -ENOMEM;
    memset(q->mem, 0, q->mem_size);
#ifdef VERBSOE
    printf("allocate cqueue %u frame, %u bytes cfrm_t size:%lu\n",
            max_frames, q->mem_size, sizeof(cfrm_t));
#endif

    f = (cfrm_t *) q->mem;
    q->free_head = f;

    for (i = 1; i < max_frames; i++)
    {
        n = (cfrm_t *)(q->mem + (i * item_size));
        n->next = NULL;
        f->next = n;
        CQ_DEBUG("%u %p -> %p\n", i, f, n);
        f = n;
        q->free_tail = n;
    }

    CQ_DEBUG("first: %p, last: %p\n",
            q->free_head, q->free_tail);
    q->free_count = max_frames;

    q->running = 1;
    return 0;
}

int cq_release(cqueue_t *q)
{
    if(q->mem) {
        free(q->mem);
    }

    pthread_mutex_destroy(&q->mutex);
    pthread_cond_destroy(&q->cond);
    memset(q, 0, sizeof(cqueue_t));
    return 0;
}


cfrm_t *cq_new_item(cqueue_t *q)
{
    cfrm_t *f = NULL;
    CENTER_FUNC();

    pthread_mutex_lock(&q->mutex);

    if (q->free_head) {
        f = q->free_head;
        q->free_head = f->next;
        if (q->free_head == NULL)
            q->free_tail = NULL;
        f->next = NULL;
        q->free_count--;
        CQ_DEBUG("new=%p ac=%u fc=%u", f, q->count, q->free_count);
    }

    pthread_mutex_unlock(&q->mutex);

    CEXIT_FUNC(0);
    return f;
}

int cq_free_item(cqueue_t *q, cfrm_t *f)
{
    CENTER_FUNC();
    pthread_mutex_lock(&q->mutex);

    f->next = NULL;
    if (q->free_tail) {
        q->free_tail->next = f;
        q->free_tail = f;
    } else {
        q->free_head = q->free_tail = f;
    }
    q->free_count++;
    CQ_DEBUG("fre=%p ac=%u fc=%u", f, q->count, q->free_count);

    pthread_mutex_unlock(&q->mutex);

    CEXIT_FUNC(0);
    return 0;
}

static inline void _cq_clear(cqueue_t *q)
{
    if (q->head && q->tail)
    {
        if (q->free_tail) {
                q->free_tail->next = q->head;
                q->free_tail = q->tail;
        } else {
            q->free_head = q->head;
            q->free_tail = q->tail;
        }
    }
    q->head = NULL;
    q->tail = NULL;
    q->count = 0;
}

void cq_clear(cqueue_t *q)
{
    pthread_mutex_lock(&q->mutex);
    _cq_clear(q);
    pthread_mutex_unlock(&q->mutex);
}

void cq_stop_loop(cqueue_t *q)
{
    pthread_mutex_lock(&q->mutex);
    _cq_clear(q);
    q->running = 0;
    pthread_cond_signal(&q->cond);
    pthread_mutex_unlock(&q->mutex);
}

cfrm_t *cq_dequeue(cqueue_t *q)
{
    cfrm_t *f = NULL;
    CENTER_FUNC();
    pthread_mutex_lock(&q->mutex);
    while(q->head == NULL)
    {
        if (q->running == 0 || q->nonblocking == 1) {
            pthread_mutex_unlock(&q->mutex);
            CEXIT_FUNC(1);
            return NULL;
        } else
            pthread_cond_wait(&q->cond, &q->mutex);
    }

    f = q->head;
    q->head = f->next;
    if (q->head == NULL)
        q->tail = NULL;

    f->next = NULL;
    q->count--;

    CQ_DEBUG("de =%p ac=%u fc=%u", f, q->count, q->free_count);

    pthread_mutex_unlock(&q->mutex);
    CEXIT_FUNC(0);
    return f;
}

int cq_enqueue(cqueue_t *q, cfrm_t *f)
{
    CENTER_FUNC();
    pthread_mutex_lock(&q->mutex);

    f->next = NULL;
    if (q->tail) {
        q->tail->next = f;
        q->tail = f;
    } else {
        q->head = q->tail = f;
    }

    q->count++;
    CQ_DEBUG("en =%p ac=%u fc=%u", f, q->count, q->free_count);
    pthread_cond_signal(&q->cond);
    pthread_mutex_unlock(&q->mutex);
    CEXIT_FUNC(0);
    return 0;
}

